var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'nazs'
});

connection.connect();

var tips = {
  title: 'another node js',
  body: 'node js and mysql is still amazing'
}

// var query = connection.query('select * from tips');
var query = connection.query('insert into tips set ?', tips, function(err, result) {
  if(err) {
    console.log(err);
    return;
  }
  console.log(result);
});
