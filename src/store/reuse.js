var rollDice = function(min,max) {
    // console.log(Math.floor(Math.random() * (max - min + 1) + min) );
     return Math.floor(Math.random() * (max - min + 1) + min);   
}

var addVal = function(el, val) {
    return el.value = parseInt(el.value) + val
}
var lessVal = function(el, val) {
    return el.value = parseInt(el.value) - val
}

var hslToHex = function (h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
        r = g = b = l; // achromatic
    } else {
        const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
        };
        const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        const p = 2 * l - q;
        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
    }
    const toHex = x => {
        const hex = Math.round(x * 255).toString(16);
        return hex.length === 1 ? '0' + hex : hex;
    };
    return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
}

var editableFrame = function(el) {
    window.frames[el].document.designMode = 'On';
    document.getElementById(el).contentDocument.body.style.fontFamily = "Arial";
    document.getElementById(el).contentDocument.body.innerHTML = "This is my sample text 123456";
}
var formatBasic = (el, att) => {
    window.frames[el].document.execCommand(att, false, null)
}
var formatMore = (el, att, opt) => {
    window.frames[el].document.execCommand(att, false, opt)
}
var setColor = function(el, c) {
    // var col = hslToHex(c, 100, 50)
    window.frames[el].document.execCommand('ForeColor', false, c)
    // console.log('test')
}
var submit_form = function() {
    var theForm = document.getElementById("myForm");

    // taking the iframe content typed by user, and put it in the textarea
    // to be submitted
    theForm.elements["myTextArea"].value = window.frames['richTextField'].document.body.innerHTML;
    theForm.submit();
}

var showForm = function(el) {
    return window.frames[el].document.body.innerHTML
}

module.exports = {
    rollDice,
    addVal,
    lessVal,
    editableFrame,
    formatBasic,
    formatMore,
    showForm,
    setColor,
    hslToHex
}

// export { 
//     rollDice,
//     addVal,
//     lessVal,
//     editableFrame,
//     formatBasic,
//     formatMore,
//     showForm,
//     setColor,
//     hslToHex    
// }


  