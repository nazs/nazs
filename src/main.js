import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Routes from './routes'
import EsPromise from 'es6-promise'
import { store } from './store/store'

// import filters from './filters'
// Vue.prototype.$http = axios;
Vue.use(VueResource);
// window.axios = axios;
Vue.use(VueRouter);
Vue.use(Vuetify);
// Vue.config.devtools = false;
export const bus = new Vue();


//filters
Vue.filter('formatDate', function(value){
  var m = new Date(value).getMonth() + 1;
  switch(m) {
    case 1:
     m = 'Jan'; break;
    case 2:
     m = 'Feb'; break;
    case 3:
     m = 'Mar'; break;
    case 4:
     m = 'Apr'; break;
    case 5:
     m = 'May'; break;
    case 6:
     m = 'Jun'; break;
    case 7:
     m = 'Jul'; break;
    case 8:
     m = 'Aug'; break;
    case 9:
     m = 'Sep'; break;
    case 10:
     m = 'Oct'; break;
    case 11:
     m = 'Nov'; break;
    case 12:
     m = 'Dec'; break;
  }
  if(value == null) {
    value = '[use calendar above]';
    // this.$store.state.dateChosen = true;
  } else {
    value = '' + new Date(value).getDate() + ' ' + m + ' ' + new Date(value).getFullYear();
  }
  // return value = value == null ? "[use calendar]" : '' + new Date(value).getDate() + ' ' + m + ' ' + new Date(value).getFullYear();
  return value;
});
Vue.filter('toUpperCase', function(value) {
return value.toUpperCase();
});
Vue.filter('toLowerCase', function(value) {
return value.toLowerCase();
});
Vue.filter('twoPlaces', function(value) {
  return value.toFixed(2);
});
Vue.filter('snippet', function(value) {
  return value.slice(0,15);
});

Vue.filter('lineBreak', function(value) {
  return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

// custom directive
Vue.directive('rainbow', {
  bind(el, binding, vnode) {
    el.style.color = "#" + Math.random().toString().slice(2,8);
  }
}); // usage: <h2 v-rainbow>Test</h2>

Vue.directive('theme', {
  bind(el, binding, vnode) {
    if(binding.value == 'wide') {
      el.style.maxWidth= "900px";
    } else if(binding.value == 'narrow') {
      el.style.maxWidth = '560px';
    };
    if(binding.arg == 'column') {
      el.style.background = '#ddd';
      el.style.color = 'purple';
    }
  }
}); // usage: <table v-theme:column="'wide'">

export const eventBus = new Vue({
  data() {
    return {
      myHost: '',
      chloie: 'my name is Chloie',
      bobbie: 'my name is Bobbie',
      food: ['milk', 'biscuits', 'crisps'],
      count: 0
    }
  },
  methods: {
    log() {
      console.log('from event bus')
    },
    myLog() {
      console.log('direct log');
      
    },
    getCount() {
      this.count++
      console.log(this.count)
    }
    ,addFood(f) {
      this.food.push(f)

      // localStorage only stores in string type
      localStorage.setItem('food', JSON.stringify(this.food))
      
      let j = JSON.parse(localStorage.getItem('food'))
      console.log(j)
    }
  },
  created() {
    this.myHost = location.hostname
    console.log(this.myHost, 'from main');
    if(localStorage.getItem('food')) {
      
      // convert to array first
      this.food = JSON.parse(localStorage.getItem('food'))
      console.log(typeof this.food)
      console.log(this.food)
    }
    else { 'storage not set'}
  }
});

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
