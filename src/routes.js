import home from './components/home.vue';
import drawer from './components/drawer.vue';
import ccna from './components/ccna.vue';
import starters from './components/menus/starters.vue';
import restaurant from './components/menus/restaurant.vue';
import sample from './components/sample.vue';
import booking from './components/booking.vue';
import chicken from './components/menus/chicken.vue';
import cart from './components/menus/cart.vue';
import beef from './components/menus/beef.vue';
import slides from './components/slides.vue';
// import locate from './components/locate.vue';
import contact from './components/contact.vue';
import login from './components/login.vue';
import noRobots from './components/noRobots.vue';
import postnew from './components/postNew.vue';
import start from './components/start.vue';
import omp from './components/omp.vue';
import obsession from './components/obsession.vue';
import cv from './components/cv/cv.vue';
import layouts from './components/layouts';
import reusables from './components/reusables.vue';
import myJournal from './components/journal.vue';
import showJournal from './components/showJournal.vue';
import flex from './components/naz/flex';
import editJournal from './components/editJournal';
import myEditor from './components/naz/wys';
import bob from './components/naz/bob';
import oneLetter from './components/naz/oneLetter';
import holiday from './components/naz/holiday'
import wys from './components/naz/wys'

export default[
  { path: '/', component: start},
  { path: '/home', component: home},
  { path: '/wysiwyg', component: myEditor},
  { path: '/bob', component: bob},
  { path: '/bob/oneLetter/:id', component: oneLetter},
  { path: '/editJournal/:id', component: editJournal },
  { path: '/myJournal', component: myJournal},
  { path: '/showJournal', component: showJournal},
  { path: '/obsession', component: obsession},
  { path: '/layouts', component: layouts},
  { path: '/reuse', component: reusables},
  { path: '/cv', component: cv},
  { path: '/drawer', component: drawer},
  { path: '/ccna', component: ccna},
  { path: '/starters', component: starters},
  { path: '/restaurant', component: restaurant},
  { path: '/sample', component: sample},
  { path: '/booking', component: booking},
  { path: '/chicken', component: chicken},
  { path: '/cart', component: cart},
  { path: '/slides', component: slides},
  // { path: '/locate', component: locate},
  { path: '/contact', component: contact},
  { path: '/login', component: login},
  { path: '/noRobots', component: noRobots},
  { path: '/postnew', component: postnew},
  { path: '/start', component: start},
  { path: '/omp', component: omp},
  { path: '/flex', component: flex},
  { path: '/wys', component: wys},
  { path: '/holiday', component: holiday},
  { path: '*', redirect: '/start'}

]
