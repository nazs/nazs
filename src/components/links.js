export default
[
  {"link": "/home", "label": "Home"},
  {"link": "/booking", "label": "Booking"},
  {"link": "/ccna", "label": "Running Config"},
  // {"link": "/slides", "label": "Slides"},
  {"link": "/restaurant", "label": "Restaurant"},
  {"link": "/noRobots", "label": "No Robots"},
  {"link": "/obsession", "label": "Obsession"}
]
